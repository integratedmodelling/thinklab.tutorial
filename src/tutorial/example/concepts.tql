namespace tutorial.example.concepts;

/* -------------------------------------------------------------------------------------------------
 * the keyword 'concept' is the most general way to define a concept. It does not say anything other
 * than "this is a concept".
 * 
 * Note the convention for naming concepts: uppercase "CamelCase" notation. Thinklab will warn you
 * if you name them with lowercase words.
 * 
 * Also note that all statements must be terminated by semicolons. Whitespace does not count; you 
 * can arrange words the way you want.
 * 
 * The editor highlights KEYWORDS in purple: these cannot be used as names for concepts, models or
 * anything else. Comments like this one can also be introduced by '//' - in that case they
 * terminate at the end of the line.
 * -------------------------------------------------------------------------------------------------
 */

concept GeneralIdea;

/* ------------------------------------------------------------------------------------------------
 * some concepts refer to OBJECTS that can exist alone. ThinkLab needs to know what things can be
 * objects, because they will be modeled as agents and not as "data". The language provides a few 
 * special keywords for object concepts.
 * -------------------------------------------------------------------------------------------------
 */

/*
 * a general "thing" can exist by itself. Note that this would not apply to, for example, 
 * temperature, which can only be the temperature OF something. Qualities need things to
 * apply to in order to make sense semantically. 
 * 
 * We interpret Food as doing nothing, so we use "thing" for it. Read on.
 */
thing Food;

/*
 * a process can also exists by itself, although its existence depends on time in a different
 * way than a "thing": things ARE while processes HAPPEN. 
 * 
 * FOR THE TECHNICALLY INCLINED: This is referred to as a PERDURANT, opposed to an ENDURANT. 
 * Read more at http://www.loa.istc.cnr.it/DOLCE.html if you are interested in the ontology 
 * of things; the DOLCE ontology is used in ThinkLab to translate these keywords.
 */
process FoodPreparation;

/*
 * an agent is a thing that may affect other things by engendering processes.
 */
agent Cook;


/* ------------------------------------------------------------------------------------------------
 * "DATA" concepts
 * 
 * If you are annotating data (meaning concepts that you can associate numbers, ranks, ratios, counts,
 * values, or classes with), they should be "qualities" and not "objects", because data need to refer
 * to an object in order to make sense. 
 * 
 * The following keywords all specify "base" physical concepts of common use for scientific data. 
 * If you use any of those, the concept are automatically recognized as qualities.
 * ThinkLab validates models and units against concepts, so you should pay attention to using
 * the right ones.
 * 
 * Note that because ThinkLab handles space and time independently, you should not worry about
 * densities or rates: if for example your spatial data represent biomass in kgs, you should use
 * mass to annotate them, not density, although the data are indeed a density because they are
 * distributed in space. The concept used for annotation should not be dependent on time or space.
 * -------------------------------------------------------------------------------------------------
 */
 
 /*
  * will be accepted in a data model, but does not tell us much.
  */
 quality GeneralQuality;
 
 /*
  * a class is expected to have concepts as values - e.g. Forest, Urban... ThinkLab won't accept
  * numbers coming from a model that observes a class.
  * 
  * Note the anticipation of how to describe a hierarchy of specialization (see below). Of course
  * this classification is very partial-  there are many more possible land use types than Forest
  * and Urban. You may want to look at the im.landcover namespace for a preview.
  */
 class LandUseType
    has children Forest, Urban;
    
 /*
  * the following are PHYSICAL PROPERTIES, all of which are represented as numbers and must be
  * associated to measurement models with units.
  * 
  * The first group are EXTENSIVE PROPERTIES - those that depend on the size of the "thing" they
  * refer to. These will be aggregated by SUMMING their value across the physical extent they 
  * refer to. See http://en.wikipedia.org/wiki/Intensive_and_extensive_properties.
  * 
  * Most of these have units associated and should be used in measurements, but some are 
  * specialized for particular types of observation (e.g. 'money' is used in 'value' statements).
  */

energy SomeKindOfEnergy;
entropy SomeKindOfEntropy;
length Bathymetry;
mass Biomass;
area LandArea;
volume WaterVolume;
money Capital;

/*
 * INTENSIVE properties are aggregated by AVERAGING across an extent. The following keywords 
 * automatically define some common observables in science and policy. Some have no units 
 * associated and can be used only in ranking (e.g. 'priority').
 */

priority DevelopmentPreference;
acceleration CarAcceleration;
charge LightingCharge;
resistance SomeResistance;
angle GeographicalSlope;
pressure AtmosphericPressure;
speed PlaneSpeed;

/*
 * Note that there are many more possible physical properties, for example electrical resistivity
 * or elasticity. Only some keywords are provided in ThinkLab for the most common properties, but
 * you can use quality and derive the ones you need from the core ontologies. This should be done
 * through the ThinkLab community process, that we will talk about later.
 */


 /* -------------------------------------------------------------------------------------------------
  * how to represent TAXONOMIES = specialization hierarchies
  * 
  * As a simple example of "ontology", here are a few concepts that specialize a common one. All the
  * different pizzas are pizzas, so a general model for pizza will apply to all of them, but a
  * special model for a given pizza type will be chosen preferentially.
  * 
  * This example also shows the following:
  * 
  * you can 'nest' full concept statements if you want to say more about a concept in a single
  * instruction. By nesting multiple statements with 'has children' you can create whole hierarchies
  * in a single statement.
  * 
  * 'disjoint' means that all the specialized concepts are independent: a MargheritaPizza can not be
  * a NeapolitanPizza etc.
  * 
  * 'with metadata' introduces a list of properties that will be associated to the concept. These
  * properties are called "annotation properties" as they do not influence the semantics but provide
  * labels, comments, etc. We will talk about those in the future.
  * -------------------------------------------------------------------------------------------------
  */
  
  thing Pizza
    has disjoint children
        MargheritaPizza,
        NeapolitanPizza,
        (thing HawaianPizza 
            with metadata { 
                flavor "ugh" 
                label "not my favorite"
            }),
        WhitePizza;
 
  
  /* ------------------------------------------------------------------------------------------------
   * MORE TO COME
   * 
   * Much more can be done with ontologies: for example, specify what the toppings for a particular
   * pizza type should be. That is done through properties and restrictions, which we will discuss
   * later.  All the information is used by ThinkLab and is important in semantic modelling, but for 
   * now we stop here. 
   * -------------------------------------------------------------------------------------------------
   */
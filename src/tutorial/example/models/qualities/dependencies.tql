namespace tutorial.example.models.qualities.dependencies 
    using im.geo, tutorial.example.data;

/*
 * this namespace includes models that show how observing a quality may depend on
 * others, and how to express those dependencies. 
 * 
 * Dependencies can be expressed by
 * linking to models if you want to make sure that a particular model will be 
 * used. However, the "standard" way should be to refer to concepts only, so that
 * the best model will be picked up at runtime, according to the context of
 * observation.
 * 
 * In such cases, Thinklab doesn't simply look for one model to satisfy a 
 * dependency, but finds ALL the models that can do so, and sorts them in 
 * order of "appropriateness" (we will talk about that is assessed later).
 * 
 * When deciding which model to use, Thinklab computes the COVERAGE of each
 * model, and if the model cannot cover the whole context (for example, data
 * are only available for a sub-region). If the total coverage is less than
 * a configurable amount (set to 95% by default), Thinklab will look for more
 * models (of lesser appropriateness) until the minimum desired coverage is
 * achieved. This way many models may be used for each dependency.
 * 
 * The RESOLUTION process is the decision made by Thinklab on how to model each
 * particular dependency. If you state the dependency using a model, the resolution
 * is done by you. If you state it through a concept, you ask Thinklab to pick
 * the best strategy to observe each dependency.
 * 
 * You can ask Thinklab to observe a CONCEPT instead of a model in the first place, by
 * "dropping" a concept on the context instead of a model. That way, Thinklab will
 * choose the model(s) that represent the best observation strategy. This should be
 * your primary modeling strategy: model for specific contexts, accurately identifying
 * the context of application, then "observe concepts" to "run" the best models for
 * your problem.
 * 
 */
 
/**
 * In this first, very useless model, we do three things:
 * 
 * 1. define the concept CrazyElevation, which automatically becomes a sub-concept of 
 *    im.geo:ElevationSeaLevel (it inherits all its properties and becomes a 'special'
 *    type of elevation);
 * 2. state that in order to observe CrazyElevation, we need to observe the slope in
 *    degrees as well.
 * 3. tell Thinklab that once the slope is known, the CrazyElevation is going to 
 *    modify the original measurement of ElevationSeaLevel (resolved from data or
 *    models as necessary) by setting it to 0 if the slope is higher than 12. This
 *    will "flatten" all areas with high slope, which makes little sense, but it's
 *    called CrazyElevation after all.
 * 
 * Things to notice:
 * 
 * 1. the dependency syntax. You can use 'observing' or 'with' - they're equivalent,
 *    sometimes the model reads more "fluent" with 'with'. After that you can state dependencies
 *    using concepts or models. If you use concepts (like below), Thinklab will find the
 *    model. Otherwise it will use the model you specify. You should only use models when
 *    you really want to create a "rigid" model which doesn't use the semantic resolution
 *    mechanism. You can add more than one dependency separated by commas (see examples
 *    below).
 * 
 * 2. each dependency can (and should) be named explicitly with an identifier, which we
 *    like to be lowercase and dash-separated like model names. That identifier can be
 *    used to refer to the dependency value within the model; it does not affect anything
 *    beyond the model it's used in. 
 * 
 * 3. the 'on definition' part makes use of the dependency by applying a transformation to
 *    the main observable (CrazyElevation) before its value is considered final. In this
 *    case, the initial value will come from resolving im.geo:ElevationSeaLevel (presumably
 *    to actual elevation values in meters). The 'on definition' intercepts that and applies
 *    a modification that depends on the slope value in the SAME POINT. Which and how many
 *    times this is run depends only on the context - for a raster grid, it will be as many
 *    times as there are cells.
 * 
 * 4. Note the syntax for expressions in square brackets. The language in there should be quite
 *    straightforward; expressions can be very complex and amount to full programs. They can use
 *    the names after 'named' to refer to the current value of the dependencies. The language
 *    they use is called Groovy and is very powerful; it's documented at groovy.codehous.org.
 *    For simple expressions, you don't need to know Groovy; it is compatible with Java and the
 *    syntax of operators is common with C and C++. We have implemented many extensions to access 
 *    the simulated world within the language which we will discuss later.
 * 
 * 5. Data models do not assume any specific context (although they can be constrained to only
 *    work in some). So the specifications below do not know about space. The value will be
 *    modified in each point of a raster if it applies to a raster context, or in each polygon
 *    if vector. This model can be applied to non-spatial contexts and will work exactly the
 *    same way, producing one number, or a timeseries of numbers if the context is spatial. 
 */
model CrazyElevation as
    measure im.geo:ElevationSeaLevel in m
    observing 
        (Slope as measure im.geo:DegreeSlope in �) named slope
    on definition
        change to 0 if [slope > 12]
;
    
        
/*
 * the "rigid" version of this model is shown below. This version ensures that the particular
 * dataset annotated in tutorial.example.data.slope-degrees will be used. The results should be 
 * identical - but if we had multiple sources of elevation, or models for them, they may differ.
 * Also, if we were running scenarios (more on that later) this version would be insensitive to
 * the choice of scenario.
 * 
 * Note the 'dot' notation for the model reference, which differs from the <namespace>:<id> one
 * we use for concepts.
 */        
model CrazyElevation named rigid-crazy-elevation as
    measure im.geo:ElevationSeaLevel in m
    observing 
        tutorial.example.data.slope-degrees named slope
    on definition
        change to 0 if [slope > 12];
        
/**
 * The model below shows a 'switching' model that uses the value of a dependency to select
 * between two different ways of observing the same observable. A model can contain any
 * number of different observers, which must be all compatible (e.g. they cannot use mix
 * observation semantics, like classifications with measurements, and they must use the same
 * units or classifications throughout).
 * 
 * This kind of condition can bring in very different models, e.g. probabilistic and 
 * deterministic, and resort to models only in the context sub-regions (in space or time)
 * that have no data available. The specification remains very simple, but this kind
 * of behavior, made possible only by adopting a semantic approach, has not been 
 * available anywhere so far. Being able for models to can change structure during execution are
 * called "structural variability" - what described here is a partial implementation of it.
 * 
 * If a model has different observers, the one that is used to observe the 'main observable'
 * (the one being modeled, in this case CrazierElevation) can be chosen by computing a 
 * condition, like below with the 'if' statement, or simply by going through a list of 
 * observers until one returns something different than 'no data' (which in Thinklab parlance
 * means 'unknown' - an actual keyword in this language). So you don't need the 'if' part
 * if you're chaining together models. 
 * 
 * This technique can be used to prioritize observation source and/or methods: you can
 * put the best observational strategy first, and add other observers below as alternatives.
 * The ones downstream will only be used if the first one was unable to observe anything. You 
 * can name alternative concepts in the different observers to provide a very powerful
 * observation strategy: Thinklab will use data or models as always, according to context.
 */
model CrazierElevation 
    
    /*
     * model dependencies - used only to select observers. Perversely,
     * it's also the observable of the two observers - only because we don't have lots
     * of training global data to play with. Try to understand how the 
     * model dependency is used: not WITHIN the observers, but only outside
     * of them to select the proper one.
     * 
     * This 'with' (or 'observing') specification appears BEFORE the 'as' and
     * is therefore used to select the dependencies for the MODEL, not for the
     * OBSERVER. These are not known by the observers ('measure') that compute
     * the numbers, but are only used in the expressions after 'if' to select 
     * which observer should compute each state. 
     */
    with (Elevation as measure im.geo:ElevationSeaLevel in m) named elevation-sea-level
    
    /*
     * two observers, both with conditionals. Parentheses are not required in this
     * case but good practice, as the 'change to' condition for each observer could be
     * wrongly attributed to the preceding observer's action if the action
     * itself was unconditional. 
     * 
     * The 'if' part outside the parentheses selects each observer according to
     * elevation. What we are saying here is that we want to use the first observer
     * only at low altitudes and the second at higher altitudes. The second observer
     * is a 'catch all' - using an 'otherwise' statement. If we had said, for example,
     * 'if [elevation-sea-level < 3000]', this would have intercepted anything at or above
     * 1000 and below 3000, and anything at or above 3000 would be 'unknown' (no-data) in
     * the results.
     */
    as
        ( measure im.geo:ElevationSeaLevel in m
            observing 
                (Slope as measure im.geo:DegreeSlope in �) named pslope1
            on definition
                change to 0 if [pslope1 < 10] )
        if [elevation-sea-level < 1000],

        ( measure im.geo:ElevationSeaLevel in m
            observing 
                (Slope as measure im.geo:DegreeSlope in �) named pslope2
            on definition
                change to 0 if [pslope2 > 10] )
        otherwise;
        
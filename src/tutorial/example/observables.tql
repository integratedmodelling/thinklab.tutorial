namespace tutorial.example.observables using im.geo, im.soil, im.valuation;

/**
 * This file tries to exemplify the difference between observation concepts and observable
 * concepts. Thinklab is very strict about proper semantic alignment of observables and
 * observations; not understanding these issues is going to cause many errors that are hard
 * to understand.
 * 
 * Observation concepts are incompatible with observable concepts and must not be mixed.
 * 
 * The OBSERVATION CONCEPT is the semantics of a MODEL. It defines the way the observable
 * concept is observed. Not all models name their observation concept explicitly, but the
 * semantics is used to validate model structure and units of measurement.
 * 
 * The OBSERVABLE CONCEPT describes the thing, process, agent or quality that the model
 * observes. 
 * 
 */
 
 
 /*
  * ----------------------------------------------------------------------------------
  * QUALITY MODELS have different forms where the two concepts have different roles. The
  * most common pattern is
  * 
  * model <OBSERVATION CONCEPT> as <observer> <OBSERVABLE CONCEPT> ...
  * 
  * In this case, the Elevation concept is defined as a measurement, and restricted to
  * be the measurement of a length. If you try to use it again in a context that is
  * incompatible (for example to measure an inclination), you will see an error.
  * ----------------------------------------------------------------------------------
  */
  
model Elevation as measure im.geo:ElevationSeaLevel in m;
    
/*
 * when the quality model annotates data directly, for example as a number or a 
 * data source, an observation concept is created by Thinklab behind the scenes.
 */
   
model 12 named ziocan as rank im.valuation:ResourceLost 1 to 100;
    
/*
 * DEPENDENCIES can be stated in two ways: with bare concepts or with models.
 * 
 * When a dependency is expressed through a bare concept, the concept must be an OBSERVATION
 * CONCEPT, because otherwise Thinklab would not know how to observe that. Observation concepts
 * are created by models, so there must be a model that defines it. If more than one is available,
 * Thinklab will choose the best one using the usual heuristics.
 */
 
 model Temperature as measure im.geo:AtmosphericTemperature in C
    observing Elevation named elevation
    using my.algorithm();
 
 
/*
 * If the dependency is stated in terms of a model, Thinklab will look for observations of
 * the OBSERVABLE CONCEPT in that model to satisfy it. This is the usual way to state a 
 * dependency.
 */
 
 model Temperature2 as measure im.geo:AtmosphericTemperature in C
    observing (Elevation as measure im.geo:ElevationSeaLevel in m) named elevation
    using my.algorithm();

 /*
  * In some models, the observation can produce more than one observable; these are always
  * stated with the 'using' form that links to an external algorithm, like a Bayesian 
  * network. In that case, the model must define all the observations that it will
  * make using observable concepts or models.
  * 
  * The first observation declared defines what the model specifies: for quality models, it corresponds
  * to what is produced by the statement after the 'as'. 
  * 
  * The observations beyond the first must be either known observation concepts (for which a 
  * model can be found) or model statements, so that Thinklab knows the semantics for all
  * additional observations.
  */
model 
        // the main observable, corresponding to the 'as' below
        SoilInfiltrationClass,
        // will lookup and find the Elevation model above
        Elevation,
        // creates PH as an observation of SoilPh
        (PH as rank im.soil:SoilPh)
    as ratio im.soil:SoilInfiltration discretized as 
        LowInfiltration  if 0 to 0.5,
        HighInfiltration if 0.5 to 1
    using bayesian( import = "..");
    
 /*
  * for SUBJECT models (which define things, agents or processes) the first concept must
  * define the OBSERVABLE: the thing, agent or process we're modeling. An appropriate
  * observation concept will be created by Thinklab.
  * 
  * The observations beyond the first follow the same rules as above, and define observations
  * of QUALITIES that will become part of this object due to the way the subject is observed.
  * The dependencies may define other qualities and subjects that will be linked to it. 
  * 
  * (commented out for lack of concepts - to rewrite with a more sensible example)
  */
  
//model WaterSupplyForResidents,
//      (ActualResidentialWaterUse   as measure aries.water:ActualResidentialUse in mm),
//      (BlockedResidentialWaterUse  as measure aries.water:BlockedResidentialUse in mm),
//      (PossibleResidentialWaterUse as measure aries.water:PossibleResidentialUse in mm)
//    observing
//        (Use as measure ResidentialWaterUse in mm) 
//              named use,
//        (Sink as measure WaterSupplySink in mm) 
//              named sink,
//        (Source as measure WaterSupplySource in mm) 
//              named source,
//        (Elevation as measure im.geo:ElevationSeaLevel in m)
//               named elevation
//    over  space(grid = "100 m")
//    using span.water-supply-flow();
/**
 * A SCENARIO is a specialized namespace that is only used to resolve dependencies when
 * it is explicitly enabled by the user. In ThinkCap, this can be done using the "Scenario"
 * view (if not visible just choose Window -> Show View -> Other... then select Scenarios, 
 * found under "Thinklab").
 * 
 * Scenarios can be defined as being alternative to other scenarios, so that two incompatible
 * scenarios are never used together. Look at tutorial.scenarios.landuse.* for an
 * example of how to define that.
 */

scenario tutorial.scenarios.crazy-elevation using im.geo;

/*
 * Models within a scenario can REINTERPRET a concept - i.e., use the same concept
 * both as the main observable and as a dependency. This is only allowed within a
 * scenario: if this same model appears in a regular namespace, an error will be
 * reported. 
 * 
 * The result is that the dependency will be resolved outside the scenario, for
 * example using data, in the usual way; then it will be transformed to the 
 * different VIEW of that concept represented in here. When this scenario is
 * enabled, any dependencies for im.geo:ElevationSeaLevel will be resolved using
 * the reinterpreted elevation below, which takes over all other models (including
 * those that annotate data).
 */
model ElevationSeaLevel as
	measure im.geo:ElevationSeaLevel in m
	observing 
		(Slope as measure im.geo:DegreeSlope in �) named slope
	on definition
		change to 0 unless [slope > 5];